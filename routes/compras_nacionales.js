const express = require('express');
const router = express.Router();

const comprasModel = require("../models/compras_nacionales");

router.get('/', function(req, res, next) {
    comprasModel
        .obtener()
        .then(compras_nacionales => {
            res.render("compras_nacionales/ver", {
                compras_nacionales: compras_nacionales,
            });
        })
        .catch(err => {
            return res.status(500).send("Error obteniendo compras nacionales");
        });

});
router.get('/agregar', function(req, res, next) {
    res.render("compras_nacionales/agregar");
});
router.post('/insertar', function(req, res, next) {
    // Obtener el nombre y precio. Es lo mismo que
    // const nombre = req.body.nombre;
    // const precio = req.body.precio;
    const { nombre, valor_compra, proveedor, cantidad, origen } = req.body;
    if (!nombre || !valor_compra || !proveedor || !cantidad || !origen) {
        return res.status(500).send("No hay datos");
    }
    // Si todo va bien, seguimos
    comprasModel
        .insertar(nombre, valor_compra, proveedor, cantidad, origen)
        .then(idCompraInsertado => {
            res.redirect("/compras_nacionales");
        })
        .catch(err => {
            return res.status(500).send("Error insertando compra nacional");
        });
});
router.get('/eliminar/:id', function(req, res, next) {
    comprasModel
        .eliminar(req.params.id)
        .then(() => {
            res.redirect("/compras_nacionales");
        })
        .catch(err => {
            return res.status(500).send("Error eliminando");
        });
});
router.get('/editar/:id', function(req, res, next) {
    comprasModel
        .obtenerPorId(req.params.id)
        .then(compra => {
            if (compra) {
                res.render("compras_nacionales/editar", {
                    compra: compra,
                });
            } else {
                return res.status(500).send("No existe compra nacional con ese id");
            }
        })
        .catch(err => {
            return res.status(500).send("Error obteniendo compra nacional");
        });
});
router.post('/actualizar/', function(req, res, next) {
    // Obtener el nombre y precio. Es lo mismo que
    // const nombre = req.body.nombre;
    // const precio = req.body.precio;
    const { id, nombre, valor_compra, proveedor, cantidad, origen } = req.body;
    if (!nombre || !valor_compra || !proveedor || !cantidad || !origen || !id) {
        return res.status(500).send("No hay suficientes datos");
    }
    // Si todo va bien, seguimos
    comprasModel
        .actualizar(id, nombre, valor_compra, proveedor, cantidad, origen)
        .then(() => {
            res.redirect("/compras_nacionales");
        })
        .catch(err => {
            return res.status(500).send("Error actualizando compra nacional");
        });
});

module.exports = router;