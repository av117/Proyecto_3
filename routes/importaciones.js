const express = require('express');
const router = express.Router();

const importacionesModel = require("../models/importaciones");

router.get('/', function(req, res, next) {
    importacionesModel
        .obtener()
        .then(importaciones => {
            res.render("importaciones/ver", {
                importaciones: importaciones,
            });
        })
        .catch(err => {
            return res.status(500).send("Error obteniendo importaciones");
        });

});
router.get('/agregar', function(req, res, next) {
    res.render("importaciones/agregar");
});
router.post('/insertar', function(req, res, next) {
    // Obtener el nombre y precio. Es lo mismo que
    // const nombre = req.body.nombre;
    // const precio = req.body.precio;
    const { nombre, valor_compra, pais_origen, proveedor, iva, costo_transporte, aduana, cantidad, origen } = req.body;
    if (!nombre || !valor_compra || !pais_origen || !proveedor || !iva || !costo_transporte || !aduana || !cantidad || !origen) {
        return res.status(500).send("No hay datos");
    }
    // Si todo va bien, seguimos
    importacionesModel
        .insertar(nombre, valor_compra, pais_origen, proveedor, iva, costo_transporte, aduana, cantidad, origen)
        .then(idImportacionInsertado => {
            res.redirect("/importaciones");
        })
        .catch(err => {
            return res.status(500).send("Error insertando importacion");
        });
});
router.get('/eliminar/:id', function(req, res, next) {
    importacionesModel
        .eliminar(req.params.id)
        .then(() => {
            res.redirect("/importaciones");
        })
        .catch(err => {
            return res.status(500).send("Error eliminando");
        });
});
router.get('/editar/:id', function(req, res, next) {
    importacionesModel
        .obtenerPorId(req.params.id)
        .then(importacion => {
            if (importacion) {
                res.render("importaciones/editar", {
                    importacion: importacion,
                });
            } else {
                return res.status(500).send("No existe importacion con ese id");
            }
        })
        .catch(err => {
            return res.status(500).send("Error obteniendo importacion");
        });
});
router.post('/actualizar/', function(req, res, next) {
    // Obtener el nombre y precio. Es lo mismo que
    // const nombre = req.body.nombre;
    // const precio = req.body.precio;
    const { id, nombre, valor_compra, pais_origen, proveedor, iva, costo_transporte, aduana, cantidad, origen } = req.body;
    if (!nombre || !valor_compra || !pais_origen || !proveedor || !iva || !costo_transporte || !aduana || !cantidad || !origen || !id) {
        return res.status(500).send("No hay suficientes datos");
    }
    // Si todo va bien, seguimos
    importacionesModel
        .actualizar(id, nombre, valor_compra, pais_origen, proveedor, iva, costo_transporte, aduana, cantidad, origen)
        .then(() => {
            res.redirect("/importaciones");
        })
        .catch(err => {
            return res.status(500).send("Error actualizando importacion");
        });
});

module.exports = router;