const express = require('express');
const listado_bodega = require('../models/listado_bodega');
const router = express.Router();

const bodegaModel = require("../models/listado_bodega");

router.get('/', function(req, res, next) {
    bodegaModel
        .obtener()
        .then(listado_bodega => {
            res.render("listado_bodega/ver", {
                listado_bodega: listado_bodega,
            });
        })
        .catch(err => {
            return res.status(500).send("Error obteniendo listado de bodega");
        });

});
router.get('/agregar', function(req, res, next) {
    res.render("listado_bodega/agregar");
});
router.get('/eliminar/:id', function(req, res, next) {
    bodegaModel
        .eliminar(req.params.id)
        .then(() => {
            res.redirect("/listado_bodega");
        })
        .catch(err => {
            return res.status(500).send("Error eliminando");
        });
});
router.get('/consultar/:id', function(req, res, next) {
    bodegaModel
        .obtenerPorId(req.params.id)
        .then(bodega => {
            if (bodega) {
                res.render("listado_bodega/consultar", {
                    bodega: bodega,
                });
            } else {
                return res.status(500).send("No existe listado con ese id");
            }
        })
        .catch(err => {
            return res.status(500).send("Error obteniendo listado bodega");
        });
});
module.exports = router;