create database proyecto3;
create table importaciones(
    id integer not null auto_increment,
    nombre varchar(64),
    valor_compra double,
	pais_origen varchar(64),
	proveedor varchar(64),
	iva varchar(64),
	costo_transporte double,
	aduana varchar(64),
	cantidad integer,
	origen varchar(64),
    primary key(id)
);

create table compra_nacional(
    id integer not null auto_increment,
    nombre varchar(64),
    valor_compra double,
	proveedor varchar(64),
	cantidad integer,
	origen varchar(64),
    primary key(id)
);
