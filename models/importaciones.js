const conexion = require("../conexion")
module.exports = {
    insertar(nombre, valor_compra, pais_origen, proveedor, iva, costo_transporte, aduana, cantidad, origen) {
        return new Promise((resolve, reject) => {
            conexion.query(`insert into importaciones
            (nombre, valor_compra, pais_origen, proveedor, iva, costo_transporte, aduana, cantidad, origen)
            values
            (?, ?, ?, ?, ?, ?, ?, ?, ?)`, [nombre, valor_compra, pais_origen, proveedor, iva, costo_transporte, aduana, cantidad, origen], (err, resultados) => {
                if (err) reject(err);
                else resolve(resultados.insertId);
            });
        });
    },
    obtener() {
        return new Promise((resolve, reject) => {
            conexion.query(`select * from importaciones`,
                (err, resultados) => {
                    if (err) reject(err);
                    else resolve(resultados);
                });
        });
    },
    obtenerPorId(id) {
        return new Promise((resolve, reject) => {
            conexion.query(`select * from importaciones where id = ?`, [id],
                (err, resultados) => {
                    if (err) reject(err);
                    else resolve(resultados[0]);
                });
        });
    },
    actualizar(id, nombre, valor_compra, pais_origen, proveedor, iva, costo_transporte, aduana, cantidad, origen) {
        return new Promise((resolve, reject) => {
            conexion.query(`update importaciones
            set nombre = ?,
            valor_compra = ?,
            pais_origen = ?,
            proveedor = ?,
            iva = ?,
            costo_transporte = ?,
            aduana = ?,
            cantidad = ?,
            origen = ?
            where id = ?`, [nombre, valor_compra, pais_origen, proveedor, iva, costo_transporte, aduana, cantidad, origen, id],
                (err) => {
                    if (err) reject(err);
                    else resolve();
                });
        });
    },
    eliminar(id) {
        return new Promise((resolve, reject) => {
            conexion.query(`delete from importaciones
            where id = ?`, [id],
                (err) => {
                    if (err) reject(err);
                    else resolve();
                });
        });
    },
}