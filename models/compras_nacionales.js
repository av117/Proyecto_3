const conexion = require("../conexion")
module.exports = {
    insertar(nombre, valor_compra, proveedor, cantidad, origen) {
        return new Promise((resolve, reject) => {
            conexion.query(`insert into compra_nacional
            (nombre, valor_compra, proveedor, cantidad, origen)
            values
            (?, ?, ?, ?, ?)`, [nombre, valor_compra, proveedor, cantidad, origen], (err, resultados) => {
                if (err) reject(err);
                else resolve(resultados.insertId);
            });
        });
    },
    obtener() {
        return new Promise((resolve, reject) => {
            conexion.query(`select * from compra_nacional`,
                (err, resultados) => {
                    if (err) reject(err);
                    else resolve(resultados);
                });
        });
    },
    obtenerPorId(id) {
        return new Promise((resolve, reject) => {
            conexion.query(`select * from compra_nacional where id = ?`, [id],
                (err, resultados) => {
                    if (err) reject(err);
                    else resolve(resultados[0]);
                });
        });
    },
    actualizar(id, nombre, valor_compra, proveedor, cantidad, origen) {
        return new Promise((resolve, reject) => {
            conexion.query(`update compra_nacional
            set nombre = ?,
            valor_compra = ?,
            proveedor = ?,
            cantidad = ?,
            origen = ?
            where id = ?`, [nombre, valor_compra, proveedor, cantidad, origen, id],
                (err) => {
                    if (err) reject(err);
                    else resolve();
                });
        });
    },
    eliminar(id) {
        return new Promise((resolve, reject) => {
            conexion.query(`delete from compra_nacional
            where id = ?`, [id],
                (err) => {
                    if (err) reject(err);
                    else resolve();
                });
        });
    },
}