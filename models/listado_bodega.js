const conexion = require("../conexion")
module.exports = {
    obtener() {
        return new Promise((resolve, reject) => {
            conexion.query(`select id, nombre, valor_compra, proveedor, cantidad, origen from importaciones
            UNION select id, nombre, valor_compra, proveedor, cantidad, origen from compra_nacional ORDER BY origen`,
                (err, resultados) => {
                    if (err) reject(err);
                    else resolve(resultados);
                });
        });
    },
    obtenerPorId(id) {
        return new Promise((resolve, reject) => {
            conexion.query(`select * from importaciones where id = ?`, [id],
                (err, resultados) => {
                    if (err) reject(err);
                    else resolve(resultados[0]);
                });
            conexion.query(`select * from compra_nacional where id = ?`, [id],
                (err, resultados) => {
                    if (err) reject(err);
                    else resolve(resultados[0]);
                });
        });
    },
    eliminar(id) {
        return new Promise((resolve, reject) => {
            conexion.query(`delete from compra_nacional where id = ?`, [id],
                (err) => {
                    if (err) reject(err);
                    else resolve();
                });
            conexion.query(`delete from importaciones where id = ?`, [id],
                (err) => {
                    if (err) reject(err);
                    else resolve();
                });
        });
    },
}